# Bismarck  snipped codes

Verifying requester matched with value in `bismarck.requesters[]`

```bash
# Mockup variables
BISMARCK_CONF=bismarck-conf.yml
WORKFLOW_EMAIL=xxx@a.com


# Core logic
ALLOW_DOMAIN=$(yq e '.bismarck.domain' $BISMARCK_CONF)
WORKFLOW_ALLOWED=$( echo $WORKFLOW_EMAIL  | grep -E ".+${ALLOW_DOMAIN}" | wc -l)

if [[ ${WORKFLOW_ALLOWED} -lt 1 ]]; then
  echo -e "[FAILED] User email [${WORKFLOW_EMAIL}] not in domain ${ALLOW_DOMAIN} ."
fi

```

Verifying approver is in accepted list

```bash

```
